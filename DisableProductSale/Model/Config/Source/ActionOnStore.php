<?php


namespace Younify\DisableProductSale\Model\Config\Source;

class ActionOnStore implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $_storeManager;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_storeManager = $storeManager;
    }

    public function toOptionArray()
    {
        $storeManagerDataList = $this->_storeManager->getStores();
        $options = array();

        foreach ($storeManagerDataList as $key => $value) {
            $options[] = [
                'value' => $key, 'label' => $value['name'].' - '.$value['code']
            ];
        }
        return $options;
    }

    public function toArray()
    {
        return ['' => __('')];
    }
}
