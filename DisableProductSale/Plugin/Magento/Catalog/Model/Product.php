<?php

namespace Younify\DisableProductSale\Plugin\Magento\Catalog\Model;

/**
 * Plugin to turn off add to cart buttons
 *
 */
class Product
{
    protected $_scopeConfig;
    protected $_storeManager;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;
    }

    /**
     * Override Product->isSaleable() to disable add to cart
     *
     * @param \Magento\Catalog\Model\Product $subject
     * @param bool $result
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterIsSalable(
        \Magento\Catalog\Model\Product $subject,
        $result
    ) {
        $isDisableActive= (bool)$this->_scopeConfig->getValue('product_special_action/config/disable_action', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $disableOnStores= explode(',', $this->_scopeConfig->getValue('product_special_action/config/disable_on_store', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
        $currentStore   = (int)$this->_storeManager->getStore()->getId();

        if ($isDisableActive && in_array($currentStore, $disableOnStores)) {
            return false;
        }
        return true;
    }
}