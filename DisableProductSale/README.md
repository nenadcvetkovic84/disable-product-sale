# Mage2 Module Younify DisableProductSale

    ``younify/module-DisableProductSale``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Enable or disable product add to cart

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Younify`
 - Enable the module by running `php bin/magento module:enable Younify_DisableProductSale`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require younify/module-DisableProductSale`
 - enable the module by running `php bin/magento module:enable Younify_DisableProductSale`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - Enable/Disabable action (product_special_action/config/enable_action)

 - Action on store (product_special_action/config/action_on_store)


## Specifications




## Attributes



